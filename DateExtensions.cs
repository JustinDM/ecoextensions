using System;

namespace Economical.EcoExtensions
{
    public static class DateExtensions
    {
        public static bool SameDay(this DateTime date, DateTime compareDate)
        {
            return (
                date.Year == compareDate.Year 
                && date.Month == compareDate.Month 
                && date.Day == compareDate.Day
            );
        }

        public static bool SameDay(this DateTime date)
        {
            var dateToday = DateTime.Now;

            return (
                date.Year == dateToday.Year
                && date.Month == dateToday.Month
                && date.Day == dateToday.Day
            );
        }

        public static bool SameMonth(this DateTime date, DateTime compareDate)
        {
            return (
                date.Year == compareDate.Year
                && date.Month == compareDate.Month
            );
        }

        public static bool SameMonth(this DateTime date)
        {
            var dateToday = DateTime.Now;

            return (
                date.Year == dateToday.Year
                && date.Month == dateToday.Month
            );
        }

        public static bool SameYear(this DateTime date, DateTime compareDate)
        {
            return (
                date.Year == compareDate.Year
            );
        }

        public static bool SameYear(this DateTime date)
        {
            var dateToday = DateTime.Now;

            return (
                date.Year == dateToday.Year
            );
        }
    }
}