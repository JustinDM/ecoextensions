using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Economical.EcoExtensions
{
    public static class StringExtensions
    {

        // checks if string contains a match for a regex string
        public static bool IsMatch(this string input, string regexString)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            var rx = new Regex(regexString);
            return rx.IsMatch(input);
        }

        // returns an array of strings that match the inputted regex string
        public static string[] GetMatches(this string input, string regexString)
        {

            if (string.IsNullOrWhiteSpace(input))
            {
                return new string[0];
            }

            var rx = new Regex(regexString);
            var matchList = new List<string>();
            var matches = rx.Matches(input);
            foreach (var i in matches)
            {
                matchList.Add(i.ToString());
            }
            return matchList.ToArray();
        }

        // checks if strings are "similar" - is one string contained in the other?
        public static bool SimilarTo(this string i, string input, bool ignoreCase = true, bool ignoreAccents = false)
        {
            if (input == null)
                input = string.Empty;

            if (i == null)
                i = string.Empty;

            var inputText = i;

            // modify inputs if required

            if (ignoreCase)
            {
                inputText = inputText.ToUpper().Replace(" ", "");
                input = input.ToUpper().Replace(" ", "");
            }
            else if (ignoreAccents)
            {
                inputText = inputText.ReplaceAccents();
                input = input.ReplaceAccents();
            }
            else
            {
                inputText = inputText.Replace(" ", "");
                input = input.Replace(" ", "");
            }

            if (inputText.Contains(input) || input.Contains(inputText))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        // returns the longest word in the string
        public static string LongestWord(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.Empty;
            }

            var inputArray = input.Split(' ');
            string longestString = "";
            foreach (var i in inputArray)
            {

                if (i.Length > longestString.Length)
                    longestString = i;
            }
            return longestString;
        }

        // Counts the number of words in the given string, returning them sorted by count, desc. Returns only words with a count greater than the minimum count
        public static Dictionary<string, int> WordCount(this string input, bool ignoreCase = false, int minCount = 0)
        {
            if (minCount < 0)
                throw new ArgumentException("Word Count: minimum word count cannot be less than 0");

            var wordDict = new Dictionary<string, int>();
            var inputText = input;

            if (ignoreCase)
                inputText = inputText.ToLower();

            var inputArray = inputText.Split(' ');
            foreach (var i in inputArray)
            {

                if (wordDict.ContainsKey(i))
                    wordDict[i] += 1;
                else
                    wordDict[i] = 1;
            }
            var sortedDict = SortDict(wordDict);
            var returnDict = new Dictionary<string, int>();
            foreach (var i in sortedDict)
            {

                if (i.Value >= minCount)
                    returnDict[i.Key] = i.Value;
            }
            return returnDict;
        }

        // sorts <string, int> dictionary by value in desc order
        private static Dictionary<string, int> SortDict(Dictionary<string, int> inputDict)
        {
            var sortedList = inputDict.OrderByDescending(kp => kp.Value).ToList();
            var sortedDict = new Dictionary<string, int>();
            foreach (var i in sortedList)
            {

                sortedDict[i.Key] = i.Value;
            }
            return sortedDict;
        }

        // counts the number of characters in a string
        public static int CharacterCount(this string input, char inputChar, bool ignoreCase = false)
        {

            var inputText = input;
            var charCount = 0;

            if (ignoreCase)
            {

                inputChar = inputChar.ToString().Trim().ToUpper().ToCharArray()[0];
                inputText = inputText.ToUpper();
            }

            foreach (var i in inputText)
            {

                if (i == inputChar)
                    charCount++;
            }

            return charCount;
        }

        public static string FillString(this string input, int finalLength, char fillCharacter)
        {
            int charsToAdd = (int)Math.Floor(((double)finalLength - (double)input.Length) / 2);

            var returnString = input.PadLeft(input.Length + charsToAdd, fillCharacter);
            returnString = returnString.PadRight(returnString.Length + charsToAdd, fillCharacter);

            if (returnString.Length < finalLength)
            {
                returnString += fillCharacter.ToString();
            }

            return returnString;
        }

        // why do I have the new() constraint?
        public static string ClassToString<T>(this T input) where T: new()
        {
            var properties = typeof(T).GetProperties();

            var displayString = $"{typeof(T)}:";

            foreach (var i in properties)
            {
                displayString += $" {i.Name}: '{i.GetValue(input)}';";
            }

            return displayString;
        }

        // take the output 
        public static T IngestClass<T>(string classString) where T: new()
        {
            // example
            // Economical.EcoObjects.CDS.ClaimReserve: Action: ; Line: Test line; Coverage: 0; Item: 0; LossType: ; LossReserve: 0; ExpReserve: 0; LossPayment: 0; ExpPayment: 0; LossPaid: 0; ExpPaid: 0; LineNumber: 0;

            var typeName = typeof(T);
            classString = classString.Replace(typeName + ":", "").Trim();

            // I think this is a unique enough indicator, but it's possible this shows up in a string - maybe a more unique seperator should be used
            var classStringItems = classString.Split(new string[] { "'; " }, StringSplitOptions.RemoveEmptyEntries);

            var classInstance = new T();

            var properties = typeof(T).GetProperties();

            foreach (var i in classStringItems)
            {
                var propertyItems = i.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

                var classProperty = properties.First(x => x.Name == propertyItems[0].Trim());

                var propertyValueString = propertyItems[1].Trim().Replace("'", "").Replace(";", "");

                object propertyValue;

                switch(true)
                {
                    case true when classProperty.PropertyType.IsEnum:
                        propertyValue = Enum.Parse(classProperty.PropertyType, propertyValueString);
                        break;

                    case true when classProperty.PropertyType == typeof(double):
                        propertyValue = Convert.ToDouble(propertyValueString);
                        break;

                    case true when classProperty.PropertyType == typeof(int):
                        propertyValue = Convert.ToInt32(propertyValueString);
                        break;

                    default:
                        propertyValue = propertyValueString;
                        break;

                }
                
                classProperty.SetValue(classInstance, propertyValue);
            }

            return classInstance;

        }

        public static bool IsEmail(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            return input.IsMatch(@"(\s|^)[a-zA-Z0-9\._-]+@[a-zA-Z0-9]+(\.|[a-zA-Z0-9])+(\s|$)");
        }

        public static string ScrubName(this string input)
        {
            var matches = input.GetMatches(@"([a-zA-Z]|\s)");

            var scrubbedName = string.Join("", matches);

            return scrubbedName;
        }

        public static string RemoveLeadingZeroes(this string input)
        {
            // need to stop once we hit a non-zero number
            var cleanString = "";

            var firstNumberReached = false;

            foreach (var i in input)
            {

                if (firstNumberReached == true)
                {
                    cleanString += i.ToString();
                }

                else if (i != '0')
                {
                    cleanString += i.ToString();
                    firstNumberReached = true;
                }
            }

            return cleanString;
        }

        public static string RetrieveFieldValue(this string noteText, string fieldName, string endFieldText)
        {
            var fieldIndex = noteText.IndexOf(fieldName);

            var removedLeadingText = noteText.Substring(fieldIndex);

            removedLeadingText = removedLeadingText.Replace(fieldName, "");

            var nextLineIndex = removedLeadingText.IndexOf(endFieldText);

            var fieldValue = removedLeadingText.Substring(0, nextLineIndex);

            return fieldValue.Trim();
        }

        // for returning strings in error messages -> limits to x characters and if something is cutoff adds '...' to the end
        public static string FormatForErrorOutput(this string field, int charLimit)
        {
            var cutField = field;

            if (cutField.Length > charLimit)
            {
                cutField = field.Substring(0, charLimit) + "...";
            }

            return cutField;
        }

        public static string Capitalize(this string input)
        {
            var inputItems = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var fixedString = string.Empty;

            foreach (var i in inputItems)
            {
                if (i.Contains('-'))
                {
                    // assuming most names only have one hypen
                    var hyphenItems = i.Split('-');

                    var firstHalf = hyphenItems[0].Capitalize();

                    var secondHalf = hyphenItems[1].Capitalize();

                    var joined = $"{firstHalf}-{secondHalf}";

                    fixedString += joined + " ";
                }
                else
                {
                    var firstChar = i[0].ToString().ToUpper();

                    var remaining = i.Substring(1).ToLower();

                    var fixedItem = firstChar + remaining;

                    fixedString += fixedItem + " ";
                }

            }

            return fixedString.Trim();
        }

        public static string ReplaceAccents(this string input)
        {
            var modifiedInput = input;

            var accents = new Dictionary<string, string>()
            {
                { "é", "e" },
                { "è", "e" },
                { "ë", "e" },
                { "ê", "e" },
                { "à", "a" },
                { "á", "a" },
                { "â", "a" },
                { "ä", "a" },
                { "ì", "i" },
                { "í", "i" },
                { "î", "i" },
                { "ï", "i" },
                { "ò", "o" },
                { "ó", "o" },
                { "ô", "o" },
                { "ö", "o" },
                { "ù", "u" },
                { "ú", "u" },
                { "û", "u" },
                { "ü", "u" }
            };

            foreach (var i in accents)
            {
                modifiedInput = modifiedInput.Replace(i.Key, i.Value);
                modifiedInput = modifiedInput.Replace(i.Key.ToUpper(), i.Value.ToUpper());
            }

            return modifiedInput;
        }

        public static bool IsPostalCode(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            return input.IsMatch(@"[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d");
        }

        public static string RemoveRepeatedLetters(this string input)
        {
            var newString = string.Empty;

            char? oldChar = null;

            foreach (char i in input)
            {
                if (!oldChar.HasValue)
                {
                    newString += i;
                }

                else if (oldChar.Value != i)
                {
                    newString += i;
                }

                oldChar = i;
                // otherwise don't do anything
            }

            return newString;
        }

        public static string CleanFileName(this string fileName)
        {
            var invalidCharacters = new string[] { "<", ">", ":", "\"", @"\", @"/", "|", "?", "*" };

            foreach (var i in invalidCharacters)
            {
                fileName = fileName.Replace(i, "");
            }

            return fileName;
        }

        public static int CalcLevDistance(this string source, string target, bool caseInsensitive = false)
        {
            if (string.IsNullOrWhiteSpace(source) && string.IsNullOrWhiteSpace(target))
            {
                return 0;
            }

            if (string.IsNullOrEmpty(source))
            {
                return target.Length;
            }

            if (string.IsNullOrEmpty(source))
            {
                return target.Length;
            }

            if (caseInsensitive)
            {
                source = source.ToUpper();
                target = target.ToUpper();
            }

            int length1 = source.Length;
            int length2 = target.Length;

            // Ensure arrays [i] / length1 use shorter length 
            if (length1 > length2)
            {
                Swap(ref target, ref source);
                Swap(ref length1, ref length2);
            }

            int maxi = length1;
            int maxj = length2;

            int[] dCurrent = new int[maxi + 1];
            int[] dMinus1 = new int[maxi + 1];
            int[] dMinus2 = new int[maxi + 1];
            int[] dSwap;

            for (int i = 0; i <= maxi; i++)
            {
                dCurrent[i] = i;
            }

            int jm1 = 0, im1 = 0, im2 = -1;

            for (int j = 1; j <= maxj; j++)
            {

                // Rotate
                dSwap = dMinus2;
                dMinus2 = dMinus1;
                dMinus1 = dCurrent;
                dCurrent = dSwap;

                // Initialize
                int minDistance = int.MaxValue;
                dCurrent[0] = j;
                im1 = 0;
                im2 = -1;

                for (int i = 1; i <= maxi; i++)
                {

                    int cost = source[im1] == target[jm1] ? 0 : 1;

                    int del = dCurrent[im1] + 1;
                    int ins = dMinus1[i] + 1;
                    int sub = dMinus1[im1] + cost;

                    //Fastest execution for min value of 3 integers
                    int min = (del > ins) ? (ins > sub ? sub : ins) : (del > sub ? sub : del);

                    if (i > 1
                        && j > 1
                        && source[im2] == target[jm1]
                        && source[im1] == target[j - 2])
                    {
                        min = Math.Min(min, dMinus2[im2] + cost);
                    }

                    dCurrent[i] = min;
                    if (min < minDistance) { minDistance = min; }
                    im1++;
                    im2++;
                }

                jm1++;

            }

            int result = dCurrent[maxi];
            return result;
        }

        private static void Swap<T>(ref T arg1, ref T arg2)
        {
            T temp = arg1;
            arg1 = arg2;
            arg2 = temp;
        }
    }

}