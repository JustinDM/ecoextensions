using System.Collections.Generic;
using System.Linq;

namespace Economical.EcoExtensions
{
    public static class CollectionExtensions
    {
        public static bool ItemPresent<T>(this IEnumerable<T> checkList, IEnumerable<T> searchItems)
        {
            // should write out method name + inputs a bit better to clarify what this does
            // compare two lists -> if any of the items in the searchItems list is present in the checkList, return true, false otherwise
            // created to pull reserve lines -> always contain a PaymentAction enum as one of the items but difficult to omit lines we don't want to parse 
            // (can't just go by alpha or numberic or a mix of both)
            foreach (T i in searchItems)
            {
                if (checkList.Contains<T>(i))
                {
                    return true;
                }
            }

            return false;
        }
        
        // public static void CleanRecords(this IEnumerable<ICleanable> dirtyRecords)
        // {
        //     if (dirtyRecords != null)
        //     {
        //         foreach (var i in dirtyRecords)
        //             i.CleanRecords();
        //     }
            
        // }
    }
}