using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace Economical.EcoExtensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum input)
        {
            Type inputEnumType = input.GetType();
            MemberInfo[] memberInfo = inputEnumType.GetMember(input.ToString());

            if ((memberInfo != null && memberInfo.Length > 0))
            {

                var _Attribs = memberInfo[0].GetCustomAttribute(typeof(System.ComponentModel.DescriptionAttribute), false);

                if ((_Attribs != null))
                {
                    return ((DescriptionAttribute)_Attribs).Description;
                }
            }

            return input.ToString();
        }

        public static string[] GetDescriptions<T>() where T : Enum
        {
            var enumItems = (T[])Enum.GetValues(typeof(T));

            var descriptionList = new List<string>();

            foreach (var i in enumItems)
                descriptionList.Add(i.GetDescription());

            return descriptionList.ToArray();
        }

        public static T ParseEnumFromDescription<T>(this string input) where T: Enum
        {
            MemberInfo[] typeInfo = typeof(T).GetFields();

            foreach (var type in typeInfo)
            {
                var attributes = (DescriptionAttribute[]) type.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0 
                    && attributes[0].Description.ToUpper() == input.ToUpper())
                {
                    return (T)Enum.Parse(typeof(T), type.Name);
                }
            }

            try
            {
                return (T)Enum.Parse(typeof(T), input, true);
            }
            catch (Exception)
            {
                throw new ArgumentException($"'{input}' could not be parsed into type '{typeof(T)}' by description or value");
            }

        }
    }
}